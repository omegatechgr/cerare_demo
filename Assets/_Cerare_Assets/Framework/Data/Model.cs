﻿using UnityEngine;

namespace Cerare.Framework.Data
{
	[System.Serializable]
	public class Model: Asset 
	{
		public string Url {get; set;}
		public float Posx {get; set;}
		public float Posy {get; set;}
		public float Posz {get; set;}
		public Vector3 Position{get; set;}

		public Model ()
		{
			DataType = DataType.Image;
		}

		public Model (string url,float x,float y,float z)
		{
			DataType = DataType.Image;
			Url = url;
			Position = new Vector3 (x, y, z);
		}
	}
}
