﻿using System.Collections.Generic;
using Cerare.Framework.Data;

namespace Cerare.Framework.Data
{
	public class State
	{
		public int FileSaveVersion{get;set;}
		public string Id{get; set;}
		public string Name{get; set;}
		public string Creator{get; set;}
		public int Preset{get;set;}
		public string Date{get; set;}
		public string Comments{get; set;}
		public List<Asset> StateAssets{get; set;}

		public int StateAssetCount { get { return StateAssets == null ? 0 : StateAssets.Count; } }

		public State()
		{
			//Default constructor
		}
	}
}