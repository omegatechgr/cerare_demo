﻿namespace Cerare.Framework.Data
{
	[System.Serializable]
	public class Image: Asset 
	{
		public string Url {get; set;}

		public Image ()
		{
			DataType = DataType.Image;
		}

		public Image (string url)
		{
			DataType = DataType.Image;
			Url = url;
		}
	}
}
