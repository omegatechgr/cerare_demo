﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Cerare.Framework.Data
{
	[System.Serializable]
	public class Video: Asset 
	{
		public string Url {get; set;}

		public Video ()
		{
			DataType = DataType.Video;
		}

		public Video (string url)
		{
			DataType = DataType.Video;
			Url = url;
		}
	}
}
