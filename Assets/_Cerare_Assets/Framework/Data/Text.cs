﻿namespace Cerare.Framework.Data
{
	[System.Serializable]
	public class Text: Asset 
	{
		public string Content {get; set;}
		public int ActorId { get; set; }

		public Text ()
		{
			DataType = DataType.Text;
		}

		public Text(string content, int actorId)
		{
			DataType = DataType.Text;
			Content = content;
			ActorId = actorId;
		}
	}
}
