﻿namespace Cerare.Framework.Data
{
	[System.Serializable]
	public class Gif: Asset 
	{
		public string Url {get; set;}

		public Gif ()
		{
			DataType = DataType.Gif;
		}

		public Gif (string url)
		{
			DataType = DataType.Gif;
			Url = url;
		}
	}
}
