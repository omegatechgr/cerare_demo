﻿using UnityEngine;
using FullSerializer;
using Cerare.Framework.Data;

namespace Cerare.Framework.Managers
{
	public class JsonManager : MonoBehaviour 
    {
		public static string JsonStatePath = "Demos/";
		public static State testState;
		private static readonly fsSerializer _serializer = new fsSerializer();

		public static State DeserializeState (string jsonname)
		{
			fsData data = fsJsonParser.Parse (TextFromStateJsonName(jsonname));
			State state = null;
			fsSerializer _serializer = new fsSerializer();
			_serializer.TryDeserialize<State> (data, ref state).AssertSuccessWithoutWarnings ();
			return state;
		}

		public static string TextFromStateJsonName (string jsonname)
		{
			string path =JsonStatePath + jsonname;
			string content = ReadStringFromDirectory (path);
			return content;
		}

		static string ReadStringFromDirectory(string path)
		{
			Debug.Log (path);
			TextAsset reader = (TextAsset)Resources.Load (path,typeof(TextAsset));
			return reader.text;
		}
	}
}
