﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Cerare.Framework.Controllers
{
    public class PresetUiController : MonoBehaviour 
    {
        public List<GameObject> Actors;
        public List<Vector3> BubblesPos;
        [SerializeField] GameObject TextPlaceHolder;
        [SerializeField] Sprite ImageDefault;
        [SerializeField] GameObject ImagePlaceHolder;
        [SerializeField] GameObject VideoPlaceHolder;
        [SerializeField] GameObject GifPlaceHolder;
        [SerializeField] UniGifImage m_uniGifImage;
        [SerializeField] string gifUrl;
        [SerializeField] public GameObject Gallery;

        private void Awake()
        {
            m_uniGifImage = GifPlaceHolder.GetComponent<UniGifImage> ();
        }
    	void ActorsListen()
    	{
    		foreach (GameObject actor in Actors) 
    		{
    			actor.GetComponent<ActorController> ().Listen ();
    		}
    	}

    	void ActorsIdle()
    	{
    		foreach (GameObject actor in Actors) 
    		{
    			actor.GetComponent<ActorController> ().Idle();
    		}
    	}

        public void ResetPreset()
        {
            ImagePlaceHolder.SetActive (true);
            ImagePlaceHolder.GetComponent<Image> ().sprite = ImageDefault;
        }

    	public void SetupText(string content,int actorid)
    	{
            Debug.Log ("SetupText");
    		ActorsListen ();
    		VideoPlaceHolder.SetActive (false);
    		TextPlaceHolder.SetActive (true);
    		Actors [actorid].GetComponent<ActorController> ().Talk ();
    		TextPlaceHolder.transform.localPosition = BubblesPos [actorid];
    		TextPlaceHolder.transform.GetChild (0).GetComponent<UnityEngine.UI.Text>().text = content;
    	}

    	public void SetupImage(string path)
    	{
            Debug.Log ("SetupImage");
    		ActorsIdle ();
    		ImagePlaceHolder.SetActive (true);
    		VideoPlaceHolder.SetActive (false);
    		TextPlaceHolder.SetActive (false);
    		Sprite sprite = Resources.Load<Sprite>(path);
    		Debug.Log (path);
    		ImagePlaceHolder.transform.GetComponent<Image> ().sprite = sprite;
    	}

        public void SetupGif(string path)
        {
            Debug.Log ("SetupGif");
            /*ActorsIdle ();
            //ImagePlaceHolder.SetActive (false);
            VideoPlaceHolder.SetActive (false);
            GifPlaceHolder.SetActive (true);
            TextPlaceHolder.SetActive (false);
            /*
            decodeGif(path);
            */
        }

        public void decodeGif(string path)
        {
            GifPlaceHolder.SetActive (true);
            string gifsPath = System.IO.Path.Combine ("Demos", "Gifs");
            gifUrl = System.IO.Path.Combine(gifsPath ,path);
            StartCoroutine (ViewGifCoroutine ());
        }

    	public void SetupVideo(string path)
    	{
    		ActorsIdle ();
    		//ImagePlaceHolder.SetActive (false);
            //GifPlaceHolder.SetActive (false);
    		VideoPlaceHolder.SetActive (true);
    		TextPlaceHolder.SetActive (false);

    		#if (UNITY_ANDROID || UNITY_IPHONE)
    		Handheld.PlayFullScreenMovie (path+".mp4", Color.black, FullScreenMovieControlMode.CancelOnInput);
    		ApplicationController.instance.NextStep();
    		#else
    		MovieTexture movie = (MovieTexture) Resources.Load(path, typeof( MovieTexture ) );

            if(movie != null)
            {
        		VideoPlaceHolder.GetComponent<RawImage> ().material.mainTexture = movie;
                VideoPlaceHolder.GetComponent<AudioSource>().clip = movie.audioClip;
        		movie.Play();
        		VideoPlaceHolder.GetComponent<AudioSource>().Play();
            }
            else
            {
                Debug.LogWarning("MovieTexture is null!");
            }
    		#endif
    	}

        private System.Collections.IEnumerator ViewGifCoroutine()
        {
            if (!m_uniGifImage.gotGif) {
                yield return StartCoroutine (m_uniGifImage.SetGifFromUrlCoroutine (gifUrl));
                ApplicationController.instance.ResetSteps ();
                ApplicationController.instance.loadingController.disableLoading ();
            } 
            else
            {
                yield return null;
                ApplicationController.instance.ResetSteps ();
                ApplicationController.instance.loadingController.disableLoading ();
            }
        }

    }
}