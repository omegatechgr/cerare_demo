﻿using System.Collections.Generic;
using UnityEngine;

namespace Cerare.Framework.Controllers
{
	public class ActorController : MonoBehaviour 
    {
        [SerializeField] List<string> animationnames;
		Animation animationcomponent = null;

		void Start()
		{
			if (GetComponent<Animation> ()) 
			{
				animationcomponent = GetComponent<Animation> ();
				foreach (AnimationState animstate in animationcomponent)
					animationnames.Add (animstate.name);
			}
		}

		public void Idle()
		{
			if(animationcomponent!=null)
				animationcomponent.CrossFade (animationnames [0]);
		}

		public void Talk()
		{
			if(animationcomponent!=null)
				animationcomponent.CrossFade (animationnames [1]);		
		}

		public void Listen()
		{
			if(animationcomponent!=null)
				animationcomponent.CrossFade (animationnames [2]);
		}
	}
}
