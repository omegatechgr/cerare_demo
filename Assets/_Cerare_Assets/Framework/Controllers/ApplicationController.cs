﻿using UnityEngine;
using Cerare.Framework.Controllers;
using Cerare.Framework.Data;

namespace Cerare.Framework.Controllers
{
	public class ApplicationController : MonoBehaviour 
    {
        public static ApplicationController instance;

        [SerializeField] GameObject NextButton;
        [SerializeField] GameObject PrevButton;
        [SerializeField] GameObject ARCamera;
        [SerializeField] UnityEngine.UI.Text CerareName;
        [SerializeField] UnityEngine.UI.Text CerareCreator;
		[SerializeField] UnityEngine.UI.Text CerareId;
        [SerializeField] string dataSetName = "";
        [SerializeField] public Cerare.Framework.UI.LoadingController loadingController;
        [SerializeField] private GameObject trackableLostanvas;
        [SerializeField] private GameObject navigationCanvas;
        [SerializeField] private GameObject restartCanvas;

        int stepindex = -1;

		void Awake()
		{
            if(GameObject.Find ("LoadingCanvas"))
                loadingController = GameObject.Find ("LoadingCanvas").GetComponent<Cerare.Framework.UI.LoadingController> ();
            
			instance = this;
            dataSetName = PlayerPrefs.GetString ("Database");

            if (loadingController)
                loadingController.disableLoading ();
		}

		public void SetActiveNavigationCanvas(bool a)
		{
            navigationCanvas.SetActive (a);
		}

		public void SetInfo(string name,string creator,string id)
		{
			CerareName.text = name;
			CerareCreator.text = creator;
			CerareId.text = id;
		}

		public void ResetSteps()
		{
            restartCanvas.SetActive (false);
            navigationCanvas.SetActive (true);
			stepindex = -1;
            PresetController.instance.CurrentPreset.GetComponent<PresetUiController> ().ResetPreset ();
            if (PresetController.instance.CurrentPreset.name.Contains ("0")) 
            {
                Debug.Log ("ResetSteps 0");
                PresetController.instance.CurrentPreset.GetComponent<PresetUiController> ().Gallery.SetActive (false);
            }
			NextStep ();
		}

		public void NextStep()
		{
            if (stepindex < StateController.instance.SelectedState.StateAssetCount-1) {
                stepindex = mod (stepindex + 1, StateController.instance.SelectedState.StateAssetCount);
                SetStep ();
            } else {
                navigationCanvas.SetActive (false);
                restartCanvas.SetActive (true);
            }
		}

        public void PrevStep ()
        {
            stepindex = mod (stepindex - 1, StateController.instance.SelectedState.StateAssetCount);
            SetStep ();
        }

		public void SetStep()
		{
			GameObject preset = PresetController.instance.CurrentPreset;
			State state = StateController.instance.SelectedState;
			Asset asset = state.StateAssets [stepindex];
			if (state.Preset != 0) {
                if (state.StateAssets [stepindex].DataType == DataType.Text) {
                    preset.GetComponent<PresetUiController> ().SetupText (((Text)asset).Content, ((Text)asset).ActorId);
                } else if (state.StateAssets [stepindex].DataType == DataType.Image) {
                    preset.GetComponent<PresetUiController> ().SetupImage (((Cerare.Framework.Data.Image)asset).Url);
                } else if (state.StateAssets [stepindex].DataType == DataType.Gif) {
                    preset.GetComponent<PresetUiController> ().SetupGif (((Cerare.Framework.Data.Gif)asset).Url);
				} else if (state.StateAssets [stepindex].DataType == DataType.Video) {
					preset.GetComponent<PresetUiController> ().SetupVideo (((Video)asset).Url);
				} else {
					NextStep ();
				}
			}
		}

        int mod(int x, int m) { return (x%m + m)%m; }

        public void showTrackableLostCanvas()
        {
            if(navigationCanvas)
                navigationCanvas.SetActive (false);
            if(trackableLostanvas)
                trackableLostanvas.SetActive (true);
        }

        public void hideTrackableLostCanvas()
        {
            if (PresetController.instance.CurrentPreset != null) {
                if (!PresetController.instance.CurrentPreset.name.Contains ("0"))
                    navigationCanvas.SetActive (true);
                else
                    navigationCanvas.SetActive (false);
            } else {
                navigationCanvas.SetActive (true);
            }
            
            trackableLostanvas.SetActive (false);
        }
	}
}
