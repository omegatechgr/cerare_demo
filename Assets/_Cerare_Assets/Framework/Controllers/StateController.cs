﻿using UnityEngine;
using Cerare.Framework.Data;

namespace Cerare.Framework.Controllers
{
	public class StateController : MonoBehaviour 
    {
        public static StateController instance;

        public State SelectedState;

		void Awake()
		{
			instance = this;
		}
            
		public void SelectState(State state)
		{
			SelectedState = state;
		}


		public Model FindModelInAssets(State state)
		{
			Model newmodel=null;
			foreach (Asset asset in state.StateAssets) 
			{
				if (asset.DataType == DataType.Model) 
				{
					newmodel = (Model)asset;
				}
			}
			return newmodel;
		}
	}
}
