﻿using UnityEngine;
using UnityEngine.UI;

namespace Cerare.Framework.Controllers
{
	public class VideoController : MonoBehaviour 
    {
		public RawImage movieimage;
		public AudioSource movieaudio;

		#if (!UNITY_ANDROID && !UNITY_IPHONE)
		public void PlayMovie()
		{
			MovieTexture movie = movieimage.material.mainTexture as MovieTexture;
			movie.Play ();
			movieaudio.Play ();

		}

		public void StopMovie()
		{
			MovieTexture movie = movieimage.material.mainTexture as MovieTexture;
			movie.Stop ();
			movieaudio.Stop ();
		}

		public void PauseMovie()
		{
			MovieTexture movie = movieimage.material.mainTexture as MovieTexture;
			movie.Pause ();
			movieaudio.Pause ();
		}
		#endif
	}
}
