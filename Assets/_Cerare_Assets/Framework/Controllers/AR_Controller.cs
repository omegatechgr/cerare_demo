﻿using System.Collections.Generic;
using UnityEngine;
using Cerare.Framework.Data;
using Cerare.Framework.Managers;

namespace Cerare.Framework.Controllers
{
	public class AR_Controller : MonoBehaviour 
    {
        public static AR_Controller instance;

        [SerializeField] List<string> JsonNames;
        [SerializeField] List<string>TrackableNames;
        [SerializeField] List<GameObject> ImageTargets;
        public State state;

		void Awake()
		{
			instance = this;
		}

		public void Initialize(string trackableName)
		{
			int i = TrackableNames.IndexOf (trackableName);
			state = JsonManager.DeserializeState (JsonNames [i]);
            //State state = JsonManager.DeserializeState(trackableName);
			StateController.instance.SelectState (state);
			PresetController.instance.SetPresetParent (ImageTargets [i], state.Preset);

			if (state.Preset == 0)
				ApplicationController.instance.SetActiveNavigationCanvas (false);
			else
				ApplicationController.instance.SetActiveNavigationCanvas (true);
			
			ApplicationController.instance.SetInfo (state.Name, state.Creator, state.Id);
            decodeData ();
		}

		public void UnlinkPreset(string trackableName)
		{
			int i = TrackableNames.IndexOf (trackableName);
			PresetController.instance.MakePresetStandalone (JsonManager.DeserializeState (JsonNames [i]).Preset);
            //PresetController.instance.MakePresetStandalone (JsonManager.DeserializeState (trackableName).Preset);

		}


        void decodeData()
        {
            State state = AR_Controller.instance.state;
            for (int i = 0; i < state.StateAssets.Count; i++) {
                if (state.StateAssets [i].DataType == DataType.Gif) 
                {
                    if (PresetController.instance.CurrentPreset.name.Contains ("0")) 
                    {
                        Debug.Log ("ResetSteps 0");
                        PresetController.instance.CurrentPreset.GetComponent<PresetUiController> ().Gallery.SetActive (true);
                    }
                    ApplicationController.instance.loadingController.enableLoading ("Processing Data");
                    Cerare.Framework.Data.Gif newGif = (Cerare.Framework.Data.Gif)state.StateAssets [i];
                    string gifPath = newGif.Url;
                    PresetController.instance.CurrentPreset.GetComponent<PresetUiController> ().decodeGif (gifPath);
                }
            }
        }

	}
}
