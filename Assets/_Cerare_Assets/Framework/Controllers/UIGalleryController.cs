﻿using UnityEngine;
using Cerare.Framework.Data;

namespace Cerare.Framework.Controllers
{
    public class UIGalleryController : MonoBehaviour 
    {
        [SerializeField] DataType SelectedDatatype;
        [SerializeField] GameObject Gallery;
        [SerializeField] GameObject GalleryText;
        [SerializeField] GameObject GalleryImage;
        [SerializeField] GameObject GalleryVideo;
        [SerializeField] GameObject GalleryGif;
        [SerializeField] GameObject GalleryTextBackground;
        [SerializeField] UniGifImage m_uniGifImage;
        [SerializeField] string gifUrl;
        [SerializeField] State state;
        [SerializeField] int step;

    	public void InitializeGallery()
    	{
    		step = -1;
    		Gallery.SetActive (true);
    		state = StateController.instance.SelectedState;
    		NextAsset ();
    	}

    	public void NextAsset()
    	{
    		Debug.Log ("Next Asset");
    		for (int i = step+1; i < state.StateAssetCount; i++) 
    		{
    			if (state.StateAssets [i].DataType == SelectedDatatype) 
    			{
    				step = i;
    				break;
    			}
    		}
    		ShowAsset ();
    	}

    	public void PreviousAsset()
    	{
    		Debug.Log ("Previous Asset");
    		for (int i = step-1; i > -1; i--) 
    		{
    			if (state.StateAssets [i].DataType == SelectedDatatype) 
    			{
    				step = i;
    				break;
    			}
    		}
    		ShowAsset ();
    	}

    	public void ShowAsset()
    	{
    		if (SelectedDatatype == DataType.Text) 
            {
    			GalleryText.SetActive (true);
    			GalleryImage.SetActive (false);
    			GalleryVideo.SetActive (false);
                GalleryGif.SetActive (false);
                GalleryTextBackground.SetActive (true);
    			GalleryText.GetComponent<UnityEngine.UI.Text> ().text = ((Cerare.Framework.Data.Text)state.StateAssets [step]).Content;
    		} 
            else if (SelectedDatatype == DataType.Image) 
            {
    			GalleryText.SetActive (false);
    			GalleryImage.SetActive (true);
    			GalleryVideo.SetActive (false);
                GalleryGif.SetActive (false);
                GalleryTextBackground.SetActive (false);
    			Sprite sprite = Resources.Load<Sprite> (((Cerare.Framework.Data.Image)state.StateAssets [step]).Url);
    			GalleryImage.GetComponent<UnityEngine.UI.Image> ().sprite = sprite;
    		}
            else if (SelectedDatatype == DataType.Gif) 
            {
                GalleryText.SetActive (false);
                GalleryImage.SetActive (false);
                GalleryVideo.SetActive (false);
                GalleryTextBackground.SetActive (false);
                GalleryGif.SetActive (true);
                /*
                 decodeGif((Cerare.Framework.Data.Gif)state.StateAssets [step]).Url);
                 */
            } 
            else
            {
    			GalleryText.SetActive (false);
    			GalleryImage.SetActive (false);
    			GalleryVideo.SetActive (true);
                GalleryTextBackground.SetActive (false);
    			GalleryVideo.GetComponent<UnityEngine.UI.Text> ().text = ((Cerare.Framework.Data.Video)state.StateAssets [step]).Url;
    		}
    	}

    	public void PlayVideo(UnityEngine.UI.Text textpath)
    	{
    		Handheld.PlayFullScreenMovie (textpath.text+".mp4", Color.black, FullScreenMovieControlMode.CancelOnInput);
    	}

        public void decodeGif(string path)
        {
            string gifsPath = System.IO.Path.Combine ("Demos", "Gifs");
            gifUrl = System.IO.Path.Combine(gifsPath ,path);
            StartCoroutine (ViewGifCoroutine ());
        }

    	public void SetDatatype(string datatype)
    	{
    		switch (datatype)
    		{
    		case "Text":
    			SelectedDatatype = DataType.Text;
    			break;
    		case "Image":
    			SelectedDatatype = DataType.Image;
    			break;
            case "Gif":
                SelectedDatatype = DataType.Gif;
                break;
    		case "Video":
    			SelectedDatatype = DataType.Video;
    			break;
    		default:
    			Debug.Log ("Datatype not recognized!");
    			break;
    		}
            if(dataExistsinAssets(SelectedDatatype))
    		    InitializeGallery ();
    	}

        private System.Collections.IEnumerator ViewGifCoroutine()
        {
            yield return StartCoroutine (m_uniGifImage.SetGifFromUrlCoroutine (gifUrl));
        }

        bool dataExistsinAssets(DataType datatype)
        {
            bool exists = false;
            State currentstate = StateController.instance.SelectedState;
            for (int i = 0; i < currentstate.StateAssets.Count; i++) 
            {
                if (currentstate.StateAssets [i].DataType == datatype) 
                {
                    exists = true;
                }
            }
            return exists;
        }
    }
}