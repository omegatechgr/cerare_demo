﻿using BarcodeScanner;
using BarcodeScanner.Scanner;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Cerare.Framework.Controllers
{
    public class ScannerController : MonoBehaviour 
    {
        private IScanner BarcodeScanner;
        public string TextHeader;
        public RawImage Image;
        private float RestartTime;

        [SerializeField]
        private Cerare.Framework.UI.LoadingController loadingController;
        [SerializeField]
        private GameObject scannerCanvas;


        void Start () {
            // Create a basic scanner
            BarcodeScanner = new Scanner();
            BarcodeScanner.Camera.Play();

            // Display the camera texture through a RawImage
            BarcodeScanner.OnReady += (sender, arg) => {
                // Set Orientation & Texture
                Image.transform.localEulerAngles = BarcodeScanner.Camera.GetEulerAngles();
                Image.transform.localScale = BarcodeScanner.Camera.GetScale();
                Image.texture = BarcodeScanner.Camera.Texture;

                // Keep Image Aspect Ratio
                var rect = Image.GetComponent<RectTransform>();
                var newHeight = rect.sizeDelta.x * BarcodeScanner.Camera.Height / BarcodeScanner.Camera.Width;
                rect.sizeDelta = new Vector2(rect.sizeDelta.x, newHeight);

                RestartTime = Time.realtimeSinceStartup;
            };
        }

        /// <summary>
        /// Start a scan and wait for the callback (wait 1s after a scan success to avoid scanning multiple time the same element)
        /// </summary>
        private void StartScanner()
        {
            BarcodeScanner.Scan((barCodeType, barCodeValue) => {
                BarcodeScanner.Stop();
                if (TextHeader.Length > 250)
                {
                    TextHeader = "";
                }
                TextHeader = barCodeValue;
                RestartTime += Time.realtimeSinceStartup + 1f;


                #if UNITY_ANDROID || UNITY_IOS
                Handheld.Vibrate();
                #endif
            });
            if (TextHeader != "") 
            {
                PlayerPrefs.SetString("Database",TextHeader);
                loadingController.enableLoading ("Loading...");
                scannerCanvas.SetActive (false);
                StartCoroutine(StopCamera(() => {
                    SceneManager.LoadScene("Scene1");
                }));

            }
        }

        /// <summary>
        /// The Update method from unity need to be propagated
        /// </summary>
        void Update()
        {
            if (BarcodeScanner != null) {
                BarcodeScanner.Update ();
            }

            // Check if the Scanner need to be started or restarted
            if (RestartTime != 0 && RestartTime < Time.realtimeSinceStartup)
            {
                StartScanner();
                RestartTime = 0;
            }
        }

        #region UI Buttons

        /// <summary>
        /// This coroutine is used because of a bug with unity (http://forum.unity3d.com/threads/closing-scene-with-active-webcamtexture-crashes-on-android-solved.363566/)
        /// Trying to stop the camera in OnDestroy provoke random crash on Android
        /// </summary>
        /// <param name="callback"></param>
        /// <returns></returns>
        public IEnumerator StopCamera(Action action)
        {
            // Stop Scanning
            BarcodeScanner.Destroy();
            BarcodeScanner = null;

            yield return new WaitForSeconds (1f);
            action.Invoke ();
        }

        #endregion
    }
}