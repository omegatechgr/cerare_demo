﻿using System.Collections.Generic;
using UnityEngine;
using Cerare.Framework.Data;

namespace Cerare.Framework.Controllers
{
	public class PresetController : MonoBehaviour 
    {
        public static PresetController instance;

        public List<GameObject> Presets;
        public GameObject CurrentPreset;
        public GameObject StateModel;

        Vector3 DefaultLocalPos = new Vector3 (0f, 0.17f, 0f);
        Vector3 DefaultLocalRot = new Vector3(0f,-180f,0f);
		
		void Awake()
		{
			StateModel = null;
			instance = this;
		}
		void UnParentPresets()
		{
			for (int i = 0; i < Presets.Count; i++) 
			{
				if (Presets [i])
					Presets [i].transform.SetParent (null);
			}
		}

		public void DisablePresets()
		{
			UnParentPresets ();
			for (int i = 0; i < Presets.Count; i++) 
			{
				if (Presets [i]) 
				{
					Presets [i].SetActive (false);
				}
			}
			DeleteModel ();
		}

		void EnablePreset(int index)
		{
			Presets [index].SetActive (true);
			foreach (GameObject actor in Presets[index].GetComponent<PresetUiController>().Actors)
				actor.GetComponent<ActorController> ().Idle ();
			CurrentPreset = Presets [index];
		}

		void AssignPreset(int index,GameObject imgTarget)
		{
			DisablePresets ();
			EnablePreset (index);
			SetPresetParent (imgTarget, index);
		}

		public void MakePresetStandalone(int index)
		{
			Presets [index].transform.SetParent (null);
			Presets [index].SetActive (true);
		}

		public void SetPresetParent(GameObject parent,int index)
		{
			DisablePresets ();
			EnablePreset (index);
			Presets [index].transform.SetParent (parent.transform);
			Presets [index].transform.localPosition = DefaultLocalPos;
			Presets [index].transform.localEulerAngles = DefaultLocalRot;
			Model model = StateController.instance.FindModelInAssets (StateController.instance.SelectedState);
			if(model!=null)
			{
				Vector3 modelpos = new Vector3(model.Posx,model.Posy,model.Posz);
				CreateModel (model.Url, modelpos);
			}
		}

		void CreateModel(string path,Vector3 pos)
		{	
			GameObject instance = Instantiate(Resources.Load(path, typeof(GameObject))) as GameObject;
			instance.transform.SetParent (CurrentPreset.transform);
			instance.transform.localPosition = pos;
			instance.transform.localEulerAngles = Vector3.zero;
			StateModel = instance;
		}

		void DeleteModel()
		{
			if (StateModel != null) 
			{
				Destroy (StateModel);
				StateModel = null;
			}
		}



	}
}
