﻿using UnityEngine;

namespace Cerare.Framework.Functionality
{
    public class CanvasLookAtCamera : MonoBehaviour
    {
    	public GameObject AR_Cam;
    	
    	// Update is called once per frame
    	void Update ()
        {
    		float x = AR_Cam.transform.eulerAngles.x;
    		float y = AR_Cam.transform.eulerAngles.y;
    		Vector3 newrot = new Vector3 (x,y,0f);
    		this.gameObject.transform.eulerAngles = newrot;
    	}
    }
}