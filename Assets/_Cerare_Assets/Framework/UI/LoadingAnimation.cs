﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Cerare.Framework.UI
{
    public class LoadingAnimation : MonoBehaviour {
        [SerializeField]
        private float loadingAnimationSpeed;
    	void Update () {
            transform.Rotate(Vector3.forward * Time.deltaTime * loadingAnimationSpeed);
    	}
    }
}
