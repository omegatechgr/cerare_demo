﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace Cerare.Framework.UI
{
    public class LoadingController : MonoBehaviour {
        
        [SerializeField]
        private Text loadingText;

        void Awake() {
            DontDestroyOnLoad(transform.gameObject);
        }

        public void enableLoading(string text)
        {
            gameObject.SetActive (true);
            loadingText.text = text;
        }

        public void disableLoading()
        {
            gameObject.SetActive (false);
        }
    }
}
