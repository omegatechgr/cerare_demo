﻿using System.Collections;
using System.Collections.Generic;
using Vuforia;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
namespace Cerare.Framework.UI
{
    public class LoadSceneButton : MonoBehaviour,IPointerClickHandler {
        [SerializeField]
        private string sceneToLoad;
        public void OnPointerClick(PointerEventData eventData)
        {
            if (sceneToLoad == "Exit") {
                Application.Quit ();
            }
            else if (sceneToLoad == "AppInit") {
                VuforiaBehaviour.Instance.enabled = false;
                SceneManager.LoadScene (sceneToLoad);
            }
            else 
            {
                SceneManager.LoadScene (sceneToLoad);
            }

        }
    }
}
