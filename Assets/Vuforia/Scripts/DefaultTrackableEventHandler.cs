/*==============================================================================
Copyright (c) 2010-2014 Qualcomm Connected Experiences, Inc.
All Rights Reserved.
Confidential and Proprietary - Protected under copyright and other laws.
==============================================================================*/

using UnityEngine;
using Cerare.Framework.Controllers;
namespace Vuforia
{
    /// <summary>
    /// A custom handler that implements the ITrackableEventHandler interface.
    /// </summary>
    public class DefaultTrackableEventHandler : MonoBehaviour,
                                                ITrackableEventHandler
    {
		public GameObject Scripts;
        #region PRIVATE_MEMBER_VARIABLES
 
        private TrackableBehaviour mTrackableBehaviour;
    
        #endregion // PRIVATE_MEMBER_VARIABLES



        #region UNTIY_MONOBEHAVIOUR_METHODS
    
        void Start()
        {
            mTrackableBehaviour = GetComponent<TrackableBehaviour>();
            if (mTrackableBehaviour)
            {
                mTrackableBehaviour.RegisterTrackableEventHandler(this);
            }
        }

        #endregion // UNTIY_MONOBEHAVIOUR_METHODS



        #region PUBLIC_METHODS

        /// <summary>
        /// Implementation of the ITrackableEventHandler function called when the
        /// tracking state changes.
        /// </summary>
        public void OnTrackableStateChanged(
                                        TrackableBehaviour.Status previousStatus,
                                        TrackableBehaviour.Status newStatus)
        {
            if (newStatus == TrackableBehaviour.Status.DETECTED ||
                newStatus == TrackableBehaviour.Status.TRACKED ||
                newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
            {
                OnTrackingFound();
            }
            else
            {
                OnTrackingLost();
            }
        }

        #endregion // PUBLIC_METHODS



        #region PRIVATE_METHODS


        private void OnTrackingFound()
        {
            if (!ApplicationController.instance.loadingController.gameObject.activeInHierarchy) {
                ApplicationController.instance.hideTrackableLostCanvas ();
                Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
                Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);

                // Enable rendering:
                foreach (Renderer component in rendererComponents)
                {
                    component.enabled = true;
                }

                // Enable colliders:
                foreach (Collider component in colliderComponents)
                {
                    component.enabled = true;
                }

                //Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " found");

    			AR_Controller.instance.Initialize (mTrackableBehaviour.TrackableName);
    			//ApplicationController.instance.ResetSteps ();
            }
        }


        private void OnTrackingLost()
        {
            Debug.Log ("Lost marker!");
            if (!ApplicationController.instance.loadingController.gameObject.activeInHierarchy) {
                ApplicationController.instance.showTrackableLostCanvas ();
                Renderer[] rendererComponents = GetComponentsInChildren<Renderer> (true);
                Collider[] colliderComponents = GetComponentsInChildren<Collider> (true);

                // Disable rendering:
                /*foreach (Renderer component in rendererComponents)
            {
                component.enabled = false;
            }

            // Disable colliders:
            foreach (Collider component in colliderComponents)
            {
                component.enabled = false;
            }*/

                //Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " lost");
                PresetController.instance.DisablePresets ();
            }
        }

        #endregion // PRIVATE_METHODS
    }
}
